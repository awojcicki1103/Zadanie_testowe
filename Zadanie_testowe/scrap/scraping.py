# import libraries
from urllib.request import urlopen
from bs4 import BeautifulSoup


def autor():
    data = []
    quote_page = 'https://teonite.com/blog/'
    older = ["/blog/"]
    for x in quote_page:
        page = urlopen(quote_page)
        soup = BeautifulSoup(page, 'html.parser')
        link = soup.find("a", class_="older-posts")
        if link == None:
            break
        else:
            link = link.get("href")
            quote_page = 'https://teonite.com' + link
            older.append(link)

    for next_page in older:
        quote_page = ['https://teonite.com' + next_page]
        for pg in quote_page:
            page = urlopen(pg)
            soup = BeautifulSoup(page, 'html.parser')
            all_links = soup.find_all("a", class_="read-more")

            for link in all_links:
                podstrona = link.get("href")
                quote_page = ['https://teonite.com' + podstrona]
                # print(quote_page)
                for pg in quote_page:
                    page = urlopen(pg)
                    soup = BeautifulSoup(page, 'html.parser')

                    for h4 in soup.findAll('h4'):
                        if h4.parent.name == 'span':
                            data.append(h4.get_text())
                            print(h4.get_text())
    return data


def tekst():
    data = []
    quote_page = 'https://teonite.com/blog/'
    older = ["/blog/"]
    for x in quote_page:
        page = urlopen(quote_page)
        soup = BeautifulSoup(page, 'html.parser')
        link = soup.find("a", class_="older-posts")
        if link == None:
            break
        else:
            link = link.get("href")
            quote_page = 'https://teonite.com' + link
            older.append(link)

    for next_page in older:
        quote_page = ['https://teonite.com' + next_page]
        for pg in quote_page:
            page = urlopen(pg)
            soup = BeautifulSoup(page, 'html.parser')
            all_links = soup.find_all("a", class_="read-more")

            for link in all_links:
                podstrona = link.get("href")
                quote_page = ['https://teonite.com' + podstrona]
                # print(quote_page)
                for pg in quote_page:
                    page = urlopen(pg)
                    soup = BeautifulSoup(page, 'html.parser')
                    # name_box = soup.find_all('section', class_= 'post-content')

                    for lines in soup.findAll('section', class_='post-content'):
                        slowa = lines.get_text()
                        slowa = slowa.split(' ')
                        slowa = [x.replace('\n', '') for x in slowa]
                        data.append(slowa)

    return data


